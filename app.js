const express = require('express'),
  path = require('path'),
  cookieParser = require('cookie-parser'),
  config = require('config'),
  port = config.get('port'),
  cors = require('cors'),
  sqlite3 = require('sqlite3'),
  fs = require('fs'),
  logger = require('morgan');

const authorization = require('./middleware/authorization'),
 indexRouter = require('./routes/index'),
 cesarRouter = require('./routes/cesar');

let app = express();
let server = require('http').createServer(app);
app.set('port', port);

/* Connexion a la base de donnée */
db = new sqlite3.Database('./db.sqlite3', (err) => {
  if (err){
    console.error(err.message);
  }
  console.log('|| Connected to the database');
});

/* view engine setup */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors({origin: config.get('client_url')}));
app.use('/', indexRouter);
/* Middleware de vérification de token avant chaque appel à la route /cesar */
app.use(authorization.verifyToken);
app.use('/cesar', cesarRouter);

/* error handler */
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

server.listen(port);
console.log('|| Cesar API server started on : ' + port);
console.log('|| Authorization API server : ' + config.get('certificate_authority_host'));

io = require('socket.io')(server);
console.log('|| Socket server on');

/* Middleware de vérification de token avant chaque appel à une socket */
io.use(authorization.verifyTokenForSocket);
io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('disconnect', function() {
    console.log('a user disconected');
    if(socket.data_batch){
      console.log(socket.data_batch.key_range_id);
      /* Si l'utilisateur se deconnecte alors qu'il était entrain de décrypter
         => rendre sa plage de clé dispo à la vérification.
       */
      let sqlUpdate = `UPDATE key_range SET status = 0 WHERE id = `+socket.data_batch.key_range_id;
      db.run(sqlUpdate, function(err){});
    }
  });

  socket.on('get_batch', function() {
      let sql = `SELECT kr.id as key_range_id, * FROM message as m INNER JOIN key_range as kr ON m.id = kr.message_id WHERE m.status = 0 AND kr.status = 0`;
      let interval = setInterval(() => {
        db.get(sql,(err, row) => {
          if(typeof row !== 'undefined'){
            let sqlUpdate = `UPDATE key_range SET status = 1 WHERE id = `+row.key_range_id;
            db.run(sqlUpdate, function(err){
              data = {
                message_id: row.message_id,
                key_range_id: row.key_range_id,
                message : row.encrypted_message,
                from_key: row.from_key,
                to_key: row.to_key
              };
              socket.join('message_'+data.message_id);
              socket.data_batch = data;
              socket.emit('set_batch', {batch: data});
              clearInterval(interval);
            });
          }
        });
      },2000);
  });

  socket.on('valid_key_found', function (data) {
    /* Récupérer le batch et le retirer du socket */
    let batch = socket.data_batch;
    delete socket.data_batch;

    /* Vérifier si la clé est bonne. */
    fs.readFile('algorithme.js', 'utf8', function(err, algoCode){
      if (!err){
        eval(algoCode);
        let resultAlgo = cesarShift(batch.message, batch.from_key, batch.to_key, config.get("validation_slug"));
        if(!resultAlgo){
          /* Mettre à jour la keyRange dans la db */
          let sqlUpdate = `UPDATE key_range SET status = 2 WHERE id = `+batch.key_range_id;
          db.run(sqlUpdate, function(err){});
          /* Quitter la room message_id */
          socket.leave('message_'+batch.message_id);
        } else {
          /* Mettre à jour la keyRange dans la db */
          let sqlUpdate = `UPDATE key_range SET status = 2 WHERE id = ?`;
          db.run(sqlUpdate, [batch.key_range_id], function(err){});
          /* Mettre à jour la message dans la db */
          let sqlUpdate2 = `UPDATE message SET status = 1, decrypted_message = ?, valid_key = ? WHERE id = ?`;
          db.run(sqlUpdate2,[data.decrypted_message, data.key, batch.message_id], function(err){
            /* Envoyer une socket cancel decryption a toute la room batch.message_id */
            socket.to('message_'+batch.message_id).emit('cancel_decryption', {decryptedMessage: data.decrypted_message, validKey: data.key, userWhoHasDecrypted: socket.decoded.name});
            /* Quitter la room message_id */
            socket.leave('message_'+batch.message_id);
          });
        }
      }
    });
  });

  socket.on('valid_key_not_found', function (data) {
    /* Récupérer le batch et le retirer du socket */
    let batch = socket.data_batch;
    delete socket.data_batch;

    /* Mettre à jour la keyRange dans la db */
    let sqlUpdate = `UPDATE key_range SET status = 2 WHERE id = `+batch.key_range_id;
    db.run(sqlUpdate, function(err){});
    /* Quitter la room message_id */
    socket.leave('message_'+batch.message_id);
  });

  socket.on('reset-bdd', function (data) {
    let sql = `UPDATE key_range SET status = 0;`;
    let sql2 = `UPDATE message SET status = 0;`;

    db.run(sql, function(err){});
    db.run(sql2, function(err){});

  })
});


module.exports = {app};
