var express = require('express');
var fs = require('fs');
var router = express.Router();
var config = require('config');


router.get('/code', function(req, res) {
  res.setHeader('Content-type', 'application/json');
  switch (req.query.lang) {
    case 'js':
    case 'javascript':
      fs.readFile('algorithme.js', 'utf8', function(err, algoCode){
        if (err){
          res.status(404);
          res.send(JSON.stringify({'code' : 'erreur'}));
        }
        else{
          res.status(200);
          res.send(JSON.stringify({'code' : algoCode}));
        }
      });
      break;
    default:
      res.status(404);
      break;
  }
});

router.get('/validation-slug', function(req, res) {
  res.setHeader('Content-type', 'application/json');
  res.send(JSON.stringify({'slug': config.get("validation_slug")}));
});

module.exports = router;