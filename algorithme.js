function cesarShift(message, fromKey, toKey, slug) {
    for(var key = fromKey; key < toKey; key++){
        var output = '';
        for (var i = 0; i < message.length; i ++) {
            var c = message[i];
            if (c.match(/[a-z]/i)) {
                var code = message.charCodeAt(i);
                if ((code >= 65) && (code <= 90)){
                    c = String.fromCharCode(((code - 65 + key) % 26) + 65);
                } else if ((code >= 97) && (code <= 122)) {
                    c = String.fromCharCode(((code - 97 + key) % 26) + 97);
                }
            }
            output += c;
        }
        if(output.indexOf(slug) != -1){
            return {"message":message, "decrypted_message": output, "key": key};
        }
    }
    return false;
};