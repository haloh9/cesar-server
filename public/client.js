let token = localStorage.removeItem("tokenCesar");
let socket, algoCode, validationSlug, batchData = null;

$( document ).ready(function() {

    if(token != null){
        $('#loginForm').remove();
        $('#cesarApp').show();
    }

    $('#submitLoginForm').on( "click", function() {
        $("#submitLoginForm").addClass('is-loading');
        /* Vider les messages d'erreurs */
        $('.help.error-form').empty();
        $('.help.error-form').removeClass('danger');

        let email = $('#loginForm #email').val();
        let password = $('#loginForm #password').val();

        if(email != "" && password != ""){
            /* Contacter l'autorité de certification */
            $.ajax({
                url: 'http://localhost:3000/auth/login',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': "Basic " + btoa(email+":"+password),
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    localStorage.setItem("tokenCesar", result.token);
                    socket = io('http://localhost:3001/',{
                        transportOptions: {
                            polling: {
                                extraHeaders: {
                                    'Authorization': 'Bearer '+result.token,
                                    'Content-Type': 'application/x-www-form-urlencoded'
                                }
                            }
                        }
                    });

                    $('#loginForm').remove();
                    $('#cesarApp').show();

                    /* Initialiser les boutons */
                    $('#getCode').attr('disabled', false);
                    $('#getValidationSlug').attr('disabled', true);
                    $('#getBatch').attr('disabled', true);
                    $('#executeAlgo').attr('disabled', true);
                },
                error: function (result) {
                    $('#loginForm #password').parents('.field').find('.error-form').html(result.message);
                    $('#loginForm #password').parents('.field').find('.error-form').addClass('is-danger');
                }
            });
        } else {
            if(email == ""){
                $('#loginForm #email').parents('.field').find('.error-form').html('This field is required');
                $('#loginForm #email').parents('.field').find('.error-form').addClass('is-danger');
            }
            if(password == ""){
                $('#loginForm #password').parents('.field').find('.error-form').html('This field is required');
                $('#loginForm #password').parents('.field').find('.error-form').addClass('is-danger');
            }
        }
        $(this).removeClass('is-loading');
    });

    $('#getCode').on("click", function () {
        token = localStorage.getItem("tokenCesar");
        /* Récupérer l'algorithme à executer */
        if(token != null){
            $.ajax({
                url: 'http://localhost:3001/cesar/code?lang=js',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': "Bearer " + token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    $('#algorithmePanel').show();
                    algoCode = result.code;
                    eval(algoCode);
                    let string = js_beautify(result.code);
                    $('#algorithmePanel .message-body pre').html(string);
                    $('#getValidationSlug').attr('disabled', false);
                    $('#getCode').attr('disabled', true);
                }
            });
        }

    });

    $('#getValidationSlug').on("click", function () {
        token = localStorage.getItem("tokenCesar");

        if(token != null){
            $.ajax({
                url: 'http://localhost:3001/cesar/validation-slug',
                type: 'GET',
                dataType: 'json',
                headers: {
                    'Authorization': "Bearer " + token,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                contentType: 'application/json; charset=utf-8',
                success: function (result) {
                    validationSlug = result.slug;
                    $('#algorithmePanel .message-body pre').prepend("var slug = "+result.slug+";");
                    $('#algorithmePanel .message-body pre').html(js_beautify($('#algorithmePanel .message-body pre').html()));
                    $('#getValidationSlug').attr('disabled', true);
                    $('#getBatch').attr('disabled', false);
                }
            });
        }

    });

    $('#getBatch').on("click", function () {
        $('#getBatch').addClass('is-loading');
        $('#msg_waiting_batch').show();
        /* Demander un batch au serveur */
        socket.emit('get_batch');

        /* Attente d'un batch depuis le serveur */
        socket.on('set_batch', function (data) {
            batchData = data.batch;
            $('#algorithmePanel .message-body pre').html(algoCode);
            $('#algorithmePanel .message-body pre').prepend("var slug = "+validationSlug+";");
            $('#algorithmePanel .message-body pre').prepend("var message = \""+data.batch.message+"\";");
            $('#algorithmePanel .message-body pre').prepend("var toKey = "+data.batch.to_key+";");
            $('#algorithmePanel .message-body pre').prepend("var fromKey = "+data.batch.from_key+";");
            $('#algorithmePanel .message-body pre').html(js_beautify($('#algorithmePanel .message-body pre').html()));
            $('#getBatch').removeClass('is-loading');
            $('#msg_waiting_batch').hide();
            $('#getBatch').attr('disabled', true);
            $('#executeAlgo').attr('disabled', false);
        });

        /* Attente d'une annulation du batch depuis le serveur */
        socket.on('cancel_decryption', function (data) {
            batchData  =  null;
            $('#getBatch').attr('disabled', false);
            $('#executeAlgo').attr('disabled', true);
            /* Initialiser la prévisualisation du code */
            $('#algorithmePanel .message-body pre').html(algoCode);
            $('#algorithmePanel .message-body pre').prepend("var slug = "+validationSlug+";");
            $('#algorithmePanel .message-body pre').html(js_beautify($('#algorithmePanel .message-body pre').html()));
            Swal.fire({
                icon: 'warning',
                title: 'Message decrypté par '+data.userWhoHasDecrypted+' avec la clé ['+data.validKey+']',
                text: data.decryptedMessage,
                showCancelButton: true,
                showConfirmButton: false,
            });
            socket.emit('valid_key_not_found');
        });
    });

    $('#executeAlgo').on("click", function () {
        /* Eval le code afin de pouvoir utiliser la methode cesarShift */
        eval(algoCode);
        let resultAlgo = cesarShift(batchData.message, batchData.from_key, batchData.to_key, validationSlug);
        batchData  =  null;
        $('#getBatch').attr('disabled', false);
        $('#executeAlgo').attr('disabled', true);
        /* Initialiser la prévisualisation du code */
        $('#algorithmePanel .message-body pre').html(algoCode);
        $('#algorithmePanel .message-body pre').prepend("var slug = "+validationSlug+";");
        $('#algorithmePanel .message-body pre').html(js_beautify($('#algorithmePanel .message-body pre').html()));
        if(resultAlgo){
            Swal.fire({
                icon: 'success',
                title: 'Bien joué ! Vous avez décrypté le message avec la clé ['+resultAlgo.key+']',
                text: resultAlgo.decrypted_message,
                showCancelButton: true,
                showConfirmButton: false,
            });
            socket.emit('valid_key_found', resultAlgo);
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Aucune clé n\'est valide, vous pouvez demander un nouveau batch',
                showCancelButton: true,
                showConfirmButton: false,
            });
            socket.emit('valid_key_not_found');
        }
    });

    $('#reset-bdd').on("click", function () {
        socket.emit('reset-bdd');
    })


});
