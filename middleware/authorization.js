const jwt = require('jsonwebtoken'),
    request = require('request'),
    config = require('config');


const verifyToken = function (req, res, next) {
    res.setHeader('Content-type', 'application/json');

    /* Vérification de la présence du Bearer Token */
    if (!req.headers.authorization || req.headers.authorization.indexOf('Bearer ') === -1) {
        return res.status(401).json({ message: 'Missing Authorization Header' });
    }

    /* Récupération du token */
    const token = req.headers.authorization.split(' ')[1];

    /* Récupération de la clé publique pour vérifier le token depui depuis l'autorité de certification */
    request(config.get('certificate_authority_host')+'/auth/publickey', function (err, response, body) {
        if (err) {
            return res.status(403).json({ message: 'The Authorization Server is not responding at the moment' });
        } else {
            let publicKey = JSON.parse(body).publicKey;
            jwt.verify(
                token,
                publicKey,
                { issuer: config.get('jwtIssuer') },
                function (err, decoded) {
                    if (err) {
                        return res.status(403).json({ message: 'The authorization token is not valid' });
                    } else {
                        req.tokenPayload = decoded;
                        next();
                    }
                });
        }
    });


}

const verifyTokenForSocket = function (socket, next) {
    let authorization = socket.handshake.headers['authorization'];
    /* Vérification de la présence du Bearer Token */
    if (!authorization || authorization.indexOf('Bearer ') === -1) {
        return next(new Error('authentication error'));
    }

    /* Récupération du token */
    const token = authorization.split(' ')[1];

    /* Récupération de la clé publique pour vérifier le token depui depuis l'autorité de certification */
    request(config.get('certificate_authority_host')+'/auth/publickey', function (err, response, body) {
        if (err) {
            return next(new Error('authentication error'));
        } else {
            let publicKey = JSON.parse(body).publicKey;
            jwt.verify(
                token,
                publicKey,
                { issuer: config.get('jwtIssuer') },
                function (err, decoded) {
                    if (err) {
                        return next(new Error('authentication error'));
                    } else {
                        socket.decoded = decoded;
                        next();
                    }
                });
        }
    });


}

module.exports = { verifyToken, verifyTokenForSocket };